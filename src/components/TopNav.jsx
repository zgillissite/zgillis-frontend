import React, { Component } from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import {Link} from 'react-router-dom';

class TopNav extends Component {
    state = {  }
    render() { 
        return ( 
            <Navbar inverse staticTop collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to="/">Zach Gillis</Link>
                    </Navbar.Brand>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={1} componentClass={Link} href="/" to="/">Home</NavItem>
                        <NavItem eventKey={2} componentClass={Link} href="/about" to="/about">About</NavItem>
                        <NavItem eventKey={3} componentClass={Link} href="/news" to="/news">News</NavItem>
                        <NavItem eventKey={4} componentClass={Link} href="/news" to="/guestbook">Guest Book</NavItem>

                    </Nav>
                </Navbar.Collapse>
            </Navbar>
         );
    }
}
 
export default TopNav;