import React, { Component, Fragment } from 'react';
import {Grid, Col, Image, Button, Table} from 'react-bootstrap';
import TopNav from './TopNav';
import GuestbookForm from './GuestbookForm';
import './css/Guestbook.css';
import axios from 'axios';


class Guestbook extends Component {
    state = { 
        entries: [],
        done: false
     }
    
    constructor(props) {
        super();
        this.getEntries();
        console.log(this.state.entries)
    }

    saveState(entries) {
        this.setState({entries});
    }

    _getEntries() {
        axios.get('http://localhost:3050/guestbook/all').then(res => {
            // const entries = res.data;
            // console.log(entries);
            this.setState({entries: res.data});
        })
    }

    getEntries = this._getEntries.bind(this);

    render() { 
        if(!this.state.done) return ( 
            <Fragment>
                <TopNav />
                <Image src='assets/travel-banner.jpg' className="header-image" />
                <Grid>
                <h3>Guest Book</h3>
                        {/* <Button onClick={this.getEntries}>Get Entries</Button> */}
                        <table className="table table-striped">
                            <thead className="thead-dark">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Message</th>
                            </thead>
                            <tbody>
                        
                            {this.state.entries.map(entry => 
                            <tr><td>{entry.nickname}</td><td>{entry.email}</td><td>{entry.message}</td></tr>)}
                        
                        </tbody>
                        </table>
                    <Col xs={12} sm={8} smOffset={2}>
                        <h2>Leave a Message</h2>
                        <GuestbookForm className="gbForm"/>
                        
                    </Col>
                    
                </Grid>
                <p className="gbUnder">Copyright © 2018, Zach Gillis</p>
            </Fragment>
         );
    }
}
 
export default Guestbook;