import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import axios from 'axios';

function FieldGroup({ id, label, help, inRef, ...props}) {
    return (
      <FormGroup controlId={id} >
        <ControlLabel>{label}</ControlLabel>
        <FormControl {...props} inputRef={inRef}/>
        {help && <HelpBlock>{help}</HelpBlock>}
      </FormGroup>
    );
  }

class GuestbookForm extends Component {
    state = { 
        
    };

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        let reqObj = {
            nickname: this.nameInput.value,
            email: this.emailInput.value,
            message: this.msgInput.value
        }
        
        axios.post('http://localhost:3050/guestbook/add', reqObj).then((res => {
            console.log(res);
        }));
        alert('Your message was posted in the guestbook.')
        window.location.reload();
    }

    render() { 
        return ( 
            <form onSubmit={this.handleSubmit}>
                <FieldGroup 
                    id="formName"
                    type="text"
                    label="Name"
                    placeholder="Enter your name"
                    inRef={input => this.nameInput = input}
                />
                <FieldGroup 
                    id="formEmail"
                    type="email"
                    label="Email Address"
                    placeholder="Enter your email address"
                    inRef={input => this.emailInput = input}
                />
                <FormGroup controlId="formMessage">
                    <ControlLabel>Message</ControlLabel>
                    <FormControl componentClass="textarea" placeholder="textarea" inputRef={input => this.msgInput = input} />
                </FormGroup>
                <Button bsStyle="primary" type="submit">Submit</Button>
            </form>
         );
    }

    clickFun() {
        alert('sdflksdjf');
    }
}
 
export default GuestbookForm;