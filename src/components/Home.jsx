import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Jumbotron, Grid, Row, Col, Image, Button } from 'react-bootstrap';
import './css/Home.css';
import TopNav from './TopNav';

class Home extends Component {
    state = {  }
    render() { 
        return ( 
            <Fragment>
                <TopNav />
                <Grid>
                    <Jumbotron>
                        <h2>Welcome to zgillis.com</h2>
                        <p>This is my new website!</p>
                        <Link to="/about">
                            <Button bsStyle="primary">About Me</Button>
                        </Link>
                    </Jumbotron>
                    <Row className="show-grid text-center">
                        <Col xs={12} sm={4} className="person-wrapper">
                            <Image src='assets/bslogo.jpg' circle className="profile-pic" />
                            <h3>ByteSiphon Channel</h3>
                            <p>Watch tech tutorials and more!</p>
                        </Col>
                        <Col xs={12} sm={4} className="person-wrapper">
                            <Image src='assets/btc.png' circle className="profile-pic" />
                            <h3>Donate Bitcoin</h3>
                            <p>This is a profile picture of the site creator.</p>
                        </Col>
                        <Col xs={12} sm={4} className="person-wrapper">
                            <Image src='assets/profie.jpg' circle className="profile-pic" />
                            <h3>About Me</h3>
                            <p>This is a profile picture of the site creator. <Link to="/about">Click here</Link>!</p>
                        </Col>
                    </Row>
                </Grid>
            </Fragment>
        );
    }
}
 
export default Home;